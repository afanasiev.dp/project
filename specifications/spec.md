# Спецификация
_________________
### Спецификация форматов обмена

* Front end ---> Back end ---> HDFS : **CSV**
* HDFS ---> Hive: **CSV**
* Hive ---> Spark: **HiveQL**
* Spark ---> Back end: **HiveQL**
* Back end ---> Front end: **Json**



###  Спецификация формата ML-модели

В качестве ML-модели будем использовать алгоритм **линейной регрессии** реализованный в пакете **Spark** **MLib**

```python
from pyspark.ml.regression import LinearRegression
```

### Спецификация формата внутренней модели

Базовые таблицы, содержащие необходимые данные:
* Таблица диагнозов и видов лечений
```
tearm(
    term_id     bigint,
    description     string)
```
* Таблица обследований пациентов 
 ```
diagnose(
    diagnose_id     bigint,
    diagnose_start_date     date,
    patient_id      bigint,
    confidence int,
    diagnose_term_id        bigint,
    diagnose_end_date       date,
    diagnose_type       int,
    diagnose_establishment_type     int)
```
* Таблица приёма препаратов
 ```
drug_intake(
    drug_intake_id     bigint,
    drug_intake_start_date     date,
    drug_intake_start_date     date,
    patient_id      bigint,
    drug_intake_type int,
    drug_term_id        bigint,
    drug_intake_count     int)
```

Рабочие таблицы:
```
tearm_diagnose(
    term_diagnose_id     bigint,
    description     string)
```
```
tearm_drug(
    term_drug_id     bigint,
    description     string)
```
```
diagnose(
    diagnose_id     bigint,
    diagnose_start_date     date,
    patient_id      bigint,
    term_diagnose_id        bigint,
    diagnose_end_date       date)
```
 ```
drug_intake(
    drug_intake_id     bigint,
    drug_intake_start_date     date,
    drug_intake_start_date     date,
    patient_id      bigint,
    term_drug_id        bigint)
```