import java.sql.DriverManager
plugins {
    java
}
group = "testId"
version = "1.0-SNAPSHOT"
repositories {
    mavenCentral()
}
dependencies{
    compile("org.apache.hive:hive-exec:3.1.1")
    compile("org.apache.hive:hive-jdbc:3.1.1")
    runtime("org.apache.hive:hive-jdbc:3.1.1")
}

buildscript{
    repositories {
        mavenCentral()
    }
    dependencies{
        classpath("org.apache.hive","hive-jdbc","3.1.1")
    }
}

tasks.register("loadDB"){
    doLast{
        try{
            Class.forName("org.apache.hive.jdbc.HiveDriver")
            //default - название бд, username/password - логин пароль
            val connection = DriverManager.getConnection("jdbc:hive2://fls3-vm:10000/default", "username", "password")
            val state = connection.createStatement()
            var respond = state.executeQuery("show tables")
            while (respond.next()) {
                println(respond.getString(1))
            }
            connection.close()
            state.close()
            respond.close()

        } catch(e: java.sql.SQLException){
            println(e.message)
        }
    }
}
